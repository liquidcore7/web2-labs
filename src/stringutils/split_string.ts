export const splitByWord = (text: string): string[] =>
  text
    .split(' ')
    .map(word => word.trim())
    .filter(word => word.length > 0);

export const randomHex = (min: number, max: number): string => {
  const diff = max - min;
  return (Math.random() * diff + min).toString(16);
};

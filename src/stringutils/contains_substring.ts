export const containsSubstring = (
  source: string,
  searched: string
): boolean => {
  const matching: RegExpMatchArray | null = source.match(`.*${searched}.*`);
  return matching != null && matching.length > 0;
};

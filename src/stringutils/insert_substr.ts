import * as _ from 'lodash';

export const insertSubstr = (
  target: string,
  atPos: number,
  suffix: string
): string => {
  const targetArr: string[] = Array.from(target);

  return _.take(targetArr, atPos)
    .concat(suffix)
    .concat(_.drop(targetArr, atPos))
    .join('');
};

import { ProjectService } from './projects/service/project_service';
import { Customer } from './projects/domain/customer';
import { Producer } from './projects/domain/producer';
import { Project } from './projects/domain/project';


const projectService = new ProjectService();

const customer = new Customer('National hospital', 'of Kyiv', 'healthcare@gov.ua');
const producer = new Producer('Software', 'company', 200, 5);
const project  = new Project('Healthcare automated platform', 750_000, 'Software');


const insertedCustomer = projectService.addCustomer(customer);
const insertedProducer = projectService.addProducer(producer);
const insertedProject  = projectService.addProject(project);

const projectId = insertedProject?.id as number;


console.log(insertedProject);

projectService.addProjectToCustomer(insertedCustomer.id as number, projectId);
projectService.addProjectToProducer(insertedProducer.id as number, projectId);

const projectWithCustomerAndProducer = projectService.findProject(projectId);

console.log(projectWithCustomerAndProducer);

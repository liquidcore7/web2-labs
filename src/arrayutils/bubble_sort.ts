export const bubbleSorted = <T>(source: T[]): T[] => {
  const sourceCopy: T[] = Array.from(source);

  for (let i = 0; i < sourceCopy.length; ++i) {
    for (let j = 1; j < sourceCopy.length - i; ++j) {
      if (sourceCopy[j - 1] > sourceCopy[j]) {
        const tmp = sourceCopy[j - 1];
        sourceCopy[j - 1] = sourceCopy[j];
        sourceCopy[j] = tmp;
      }
    }
  }
  return sourceCopy;
};

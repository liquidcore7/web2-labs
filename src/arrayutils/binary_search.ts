export const binarySearch = <T>(source: T[], searched: T): number | null => {
  function binarySearchImpl(startIdx: number, endIdx: number): number | null {
    const length: number = endIdx - startIdx;

    if (length === 0) {
      return null;
    }

    const mid = Math.floor(length / 2) + startIdx;

    const midElem: T = source[mid];

    if (searched === midElem) return mid;
    else if (searched > midElem) {
      return binarySearchImpl(mid, endIdx);
    } else return binarySearchImpl(startIdx, mid);
  }

  return binarySearchImpl(0, source.length);
};

export const maxElement = <T>(
  source: T[],
  maybeLT?: (lhs: T, rhs: T) => boolean
): T => {
  const lt = maybeLT === undefined ? (a: T, b: T): boolean => a < b : maybeLT;
  return source.reduce((a: T, b: T) => (lt(a, b) ? b : a));
};

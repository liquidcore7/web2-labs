export const factorial = (of: number): number =>
  of <= 1 ? 1 : of * factorial(of - 1);

import { Entity } from '../domain/entity';
import * as _ from 'lodash';
import { CrudRepo } from './crud_repo';

export class InMemoryRepo<T extends Entity> implements CrudRepo<T> {
  private readonly elements: Array<T | null>;
  private readonly droppedIds: number[] = [];

  constructor(of?: T[]) {
    this.elements = _.map(
      { ...of } || {},
      (value: T, key: string) => value.withId(Number(key).valueOf()) as T
    );
  }

  findById(id: number): T | null {
    return this.elements.length > id ? this.elements[id] : null;
  }

  create(elem: T): T {
    const generatedId: number = this.droppedIds.pop() || this.elements.length;
    const entityWithId: T = elem.withId(generatedId) as T;

    if (generatedId < this.elements.length) {
      this.elements[generatedId] = entityWithId;
    } else this.elements.push(entityWithId);

    return entityWithId;
  }

  deleteById(id: number): void {
    this.droppedIds.push(id);
    this.elements[id] = null;
  }

  update(oldId: number, newElem: T): T | null {
    const oldElem: T | null = this.findById(oldId);
    if (oldElem != null) {
      const withCorrectId: T = newElem.withId(oldId) as T;
      this.elements[oldId] = withCorrectId;
      return withCorrectId;
    }
    return oldElem;
  }

  fetchAll(): T[] {
    return this.elements
      .filter(entity => entity != null)
      .map(entity => entity as T);
  }
}

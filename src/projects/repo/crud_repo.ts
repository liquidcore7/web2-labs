import { Entity } from '../domain/entity';

export interface CrudRepo<T extends Entity> {
  findById(id: number): T | null;

  create(elem: T): T;

  deleteById(id: number): void;

  update(oldId: number, newElem: T): T | null;

  fetchAll(): T[];
}

import { InMemoryRepo } from '../repo/in_memory_repo';
import { Customer } from '../domain/customer';
import { Producer } from '../domain/producer';
import { CrudRepo } from '../repo/crud_repo';
import { Project } from '../domain/project';
import { NumericDictionary, Primitive } from 'lodash';
import { ExpandedProject } from '../domain/expanded_project';

export class ProjectService {
  private readonly customerRepo: CrudRepo<Customer> = new InMemoryRepo<
    Customer
  >();
  private readonly producerRepo: CrudRepo<Producer> = new InMemoryRepo<
    Producer
  >();
  private readonly projectRepo: CrudRepo<Project> = new InMemoryRepo<Project>();

  private readonly customerProjectMapping: NumericDictionary<Customer> = {};
  private readonly producerProjectMapping: NumericDictionary<Producer> = {};

  /* -- CUSTOMER SECTION -- */
  findCustomer(byId: number): Customer | null {
    return this.customerRepo.findById(byId);
  }

  addCustomer(customer: Customer): Customer {
    return this.customerRepo.create(customer);
  }

  removeCustomer(customerId: number): void {
    this.customerRepo.deleteById(customerId);
  }

  updateCustomer(oldId: number, newCustomer: Customer): Customer | null {
    return this.customerRepo.update(oldId, newCustomer);
  }

  getCustomers(): Customer[] {
    return this.customerRepo.fetchAll();
  }

  /* -- PRODUCER SECTION -- */
  findProducer(byId: number): Producer | null {
    return this.producerRepo.findById(byId);
  }

  addProducer(producer: Producer): Producer {
    return this.producerRepo.create(producer);
  }

  removeProducer(producerId: number): void {
    this.producerRepo.deleteById(producerId);
  }

  updateProducer(oldId: number, newProducer: Producer): Producer | null {
    return this.producerRepo.update(oldId, newProducer);
  }

  getProducers(): Producer[] {
    return this.producerRepo.fetchAll();
  }

  /* -- PROJECT SECTION -- */
  private makeExpandedProject(fromProject: Project): ExpandedProject | null {
    const projectId = fromProject.id;
    if (projectId == null) {
      return null;
    }

    const maybeCustomer: Customer | undefined = this.customerProjectMapping[
      projectId
    ];
    const maybeProducer: Producer | undefined = this.producerProjectMapping[
      projectId
    ];

    return new ExpandedProject(
      projectId as number,
      fromProject.name,
      fromProject.price,
      fromProject.domain,
      maybeCustomer,
      maybeProducer
    );
  }

  findProject(byId: number): ExpandedProject | null {
    const maybeProject: Project | null = this.projectRepo.findById(byId);
    return maybeProject == null
      ? null
      : this.makeExpandedProject(maybeProject as Project);
  }

  rawAddProject(project: Project): Project {
    return this.projectRepo.create(project);
  }

  addProject(project: Project): ExpandedProject | null {
    const insertedProject = this.rawAddProject(project);
    return this.makeExpandedProject(insertedProject);
  }

  removeProject(projectId: number): void {
    delete this.producerProjectMapping[projectId];
    delete this.customerProjectMapping[projectId];

    this.projectRepo.deleteById(projectId);
  }

  updateProject(oldId: number, newProject: Project): ExpandedProject | null {
    const updatedProject: Project | null = this.projectRepo.update(
      oldId,
      newProject
    );
    return updatedProject == null
      ? null
      : this.makeExpandedProject(updatedProject as Project);
  }

  getProjects(): ExpandedProject[] {
    return this.projectRepo
      .fetchAll()
      .map(this.makeExpandedProject, this)
      .filter(expProject => expProject != null)
      .map(
        (maybeExpProject: ExpandedProject | null) =>
          maybeExpProject as ExpandedProject
      );
  }

  addProjectToCustomer(
    customerId: number,
    projectId: number
  ): ExpandedProject | null {
    const customer: Customer | null = this.findCustomer(customerId);
    if (customer != null) {
      this.customerProjectMapping[projectId] = customer as Customer;
    }

    return this.findProject(projectId);
  }

  addProjectToProducer(
    producerId: number,
    projectId: number
  ): ExpandedProject | null {
    const producer: Producer | null = this.findProducer(producerId);
    if (producer != null) {
      this.producerProjectMapping[projectId] = producer as Producer;
    }

    return this.findProject(projectId);
  }
}

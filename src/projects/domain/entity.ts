export interface Entity {
  id?: number;

  withId(newId: number): Entity;
}

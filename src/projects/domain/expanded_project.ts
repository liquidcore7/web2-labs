import { Producer } from './producer';
import { Customer } from './customer';
import { Project } from './project';

export class ExpandedProject {
  readonly id: number;
  readonly name: string;
  readonly price: number;
  readonly domain: string;
  readonly customer?: Customer;
  readonly producer?: Producer;

  constructor(
    id: number,
    name: string,
    price: number,
    domain: string,
    customer?: Customer,
    producer?: Producer
  ) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.domain = domain;
    this.customer = customer;
    this.producer = producer;
  }

  toProject(): Project {
    return new Project(this.name, this.price, this.domain, this.id);
  }
}

import { Entity } from './entity';

export class Producer implements Entity {
  id?: number;
  readonly domain: string;
  readonly type: 'company' | 'person';
  readonly employees: number;
  readonly rating: number;

  constructor(
    domain: string,
    type: 'company' | 'person',
    employees: number,
    rating: number,
    id?: number
  ) {
    this.id = id;
    this.domain = domain;
    this.type = type;
    this.employees = employees;
    this.rating = rating;
  }

  withId(newId: number): Producer {
    this.id = newId;
    return this;
  }
}

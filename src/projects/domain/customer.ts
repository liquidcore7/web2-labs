import { Entity } from './entity';

export class Customer implements Entity {
  id?: number;
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;

  constructor(firstName: string, lastName: string, email: string, id?: number) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }

  withId(newId: number): Customer {
    this.id = newId;
    return this;
  }
}

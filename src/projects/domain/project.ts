import { Entity } from './entity';

export class Project implements Entity {
  id?: number;
  readonly name: string;
  readonly price: number;
  readonly domain: string;

  constructor(name: string, price: number, domain: string, id?: number) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.domain = domain;
  }

  withId(newId: number): Project {
    this.id = newId;
    return this;
  }
}

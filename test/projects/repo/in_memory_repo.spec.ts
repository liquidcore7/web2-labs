import { Entity } from '../../../src/projects/domain/entity';
import { InMemoryRepo } from '../../../src/projects/repo/in_memory_repo';
import * as assert from 'assert';
import * as _ from 'lodash';


class JustId implements Entity {
    id?: number;

    constructor(id: number) {
        this.id = id;
    }
    
    withId(newId: number): JustId {
        this.id = newId;
        return this;
    }
}


describe('In-memory repo', () => {
    it('should be created from an empty array', () => {
        assert.deepStrictEqual(new InMemoryRepo().fetchAll(), []);
    });
    it('should be created from some given array', () => {
        const threeIds = [1, 2, 3].map(id => new JustId(id));
        assert.deepStrictEqual(
            new InMemoryRepo(threeIds).fetchAll(), 
            [0, 1, 2].map(id => new JustId(id))
        );
    });
    it('should insert elements', () => {
        const emptyRepo = new InMemoryRepo<JustId>();
        const testData = [1, 2].map(id => new JustId(id));

        assert.deepStrictEqual(emptyRepo.fetchAll(), []);

        testData.forEach(justId => emptyRepo.create(justId));
        assert.deepStrictEqual(emptyRepo.fetchAll(), testData);
    });
    it('should find elements by id', () => {
        const repo = new InMemoryRepo([new JustId(0), new JustId(1)]);
        assert.equal(repo.findById(1), _.last(repo.fetchAll()));
    });
    it('should return null when searching for non-existent element', () => {
        const emptyRepo = new InMemoryRepo<JustId>();
        assert.equal(emptyRepo.findById(1), null);
    });
    it('should delete existing elements', () => {
        const repo = new InMemoryRepo([0, 1, 2, 3, 4].map(id => new JustId(id)));

        [0, 1, 3].forEach(id => repo.deleteById(id));

        assert.deepStrictEqual(repo.fetchAll(), [new JustId(2), new JustId(4)]);
    });
});

import * as assert from 'assert';

import { Customer } from '../../../src/projects/domain/customer';
import { Producer } from '../../../src/projects/domain/producer';
import { Project }  from '../../../src/projects/domain/project';

import { ProjectService } from '../../../src/projects/service/project_service';
import { Entity } from '../../../src/projects/domain/entity';
import { ExpandedProject } from '../../../src/projects/domain/expanded_project';


const StubObjects = {
    customer: new Customer('Tax service', 'of Lviv Oblast', 'tax@gov.ua'),
    customer2: new Customer('Tax service', 'of Kiev', 'tax@gov.ua'),
    producer: new Producer('Software engineering', 'company', 2, 5),
    producer2: new Producer('Software engineering', 'person', 0, 4),
    project:  new Project('Digital taxing platform', 3_500_000, 'Software engineering'),
    project2: new Project('Tax HRMS', 30_000, 'Software engineering')
};

const PSLambdas = {
    addCustomer: (ps: ProjectService, c: Customer): Customer => ps.addCustomer(c),
    addProducer: (ps: ProjectService, p: Producer): Producer => ps.addProducer(p),
    addProject:  (ps: ProjectService, p: Project): Project => ps.rawAddProject(p),

    findCustomer: (ps: ProjectService, id: number): Customer | null => ps.findCustomer(id),
    findProducer: (ps: ProjectService, id: number): Producer | null => ps.findProducer(id),
    findProject: (ps: ProjectService, id: number): Project | null => {
        const expanded = ps.findProject(id);
        return expanded == null ? null : expanded.toProject();
    },

    fetchCustomers: (ps: ProjectService): Customer[] => ps.getCustomers(),
    fetchProducers: (ps: ProjectService): Producer[] => ps.getProducers(),
    fetchProjects:  (ps: ProjectService): Project[] => ps.getProjects().map(expanded => expanded.toProject()),

    deleteCustomer: (ps: ProjectService, id: number): void => ps.removeCustomer(id),
    deleteProducer: (ps: ProjectService, id: number): void => ps.removeProducer(id),
    deleteProject:  (ps: ProjectService, id: number): void => ps.removeProject(id),

    updateCustomer: (ps: ProjectService, oldId: number, newCustomer: Customer): Customer | null => ps.updateCustomer(oldId, newCustomer),
    updateProducer: (ps: ProjectService, oldId: number, newProducer: Producer): Producer | null => ps.updateProducer(oldId, newProducer),
    updateProject:  (ps: ProjectService, oldId: number, newProject: Project): Project | null => {
        const updated = ps.updateProject(oldId, newProject);
        return updated == null ? null : updated.toProject();
    }

};



const testInsert = <T extends Entity>(entity: T, createF: (ps: ProjectService, entity: T) => T): void => {
    const projectService = new ProjectService();
    const insertedEntity = createF(projectService, entity);

    assert.notEqual(insertedEntity.id, undefined);

    assert.deepStrictEqual(insertedEntity, entity);
};

const testFetch = <T extends Entity>(entity: T, 
                                     createF: (ps: ProjectService, entity: T) => T,
                                     findByIdF: (ps: ProjectService, id: number) => T | null,
                                     fetchAllF: (ps: ProjectService) => T[]): void => {
    const projectService = new ProjectService();

    const entity1 = createF(projectService, entity);
    const entity2 = createF(projectService, entity);

    assert.deepStrictEqual(entity2, findByIdF(projectService, entity2.id as number));
    assert.deepStrictEqual([entity1, entity2], fetchAllF(projectService));
};

const testDelete = <T extends Entity>(entity: T,
                                      createF: (ps: ProjectService, entity: T) => T,
                                      findByIdF: (ps: ProjectService, id: number) => T | null,
                                      deleteF: (ps: ProjectService, id: number) => void): void => {
    const projectService = new ProjectService();
    const inserted = createF(projectService, entity);

    assert.notEqual(findByIdF(projectService, inserted.id as number), null);

    deleteF(projectService, inserted.id as number);
    
    assert.equal(findByIdF(projectService, inserted.id as number), null);
};

const testUpdate = <T extends Entity>(entity1: T, entity2: T,
                                      createF: (ps: ProjectService, entity: T) => T,
                                      findByIdF: (ps: ProjectService, id: number) => T | null,
                                      updateF: (ps: ProjectService, oldId: number, newEntity: T) => T | null): void => {
    const projectService = new ProjectService();

    const inserted = createF(projectService, entity1);
    const updated = updateF(projectService, inserted.id as number, entity2);

    assert.deepStrictEqual(updated, entity2.withId(inserted.id as number));
}



describe('ProjectService', () => {
    describe('should support create operation', () => {
        it('for customer', () => testInsert(StubObjects.customer, PSLambdas.addCustomer));
        it('for producer', () => testInsert(StubObjects.producer, PSLambdas.addProducer));
        it('for project',  () => testInsert(StubObjects.project,  PSLambdas.addProject));
    });
    describe('should support findById and fetchAll operations', () => {
        it('for customer', () => testFetch(
            StubObjects.customer, 
            PSLambdas.addCustomer,
            PSLambdas.findCustomer,
            PSLambdas.fetchCustomers
        ));
        it('for producer', () => testFetch(
            StubObjects.producer,
            PSLambdas.addProducer,
            PSLambdas.findProducer,
            PSLambdas.fetchProducers
        ));
        it('for project', () => testFetch(
            StubObjects.project,
            PSLambdas.addProject,
            PSLambdas.findProject,
            PSLambdas.fetchProjects
        ));
    });
    describe('should support delete operation', () => {
        it('for customer', () => testDelete(
            StubObjects.customer,
            PSLambdas.addCustomer,
            PSLambdas.findCustomer,
            PSLambdas.deleteCustomer
        ));
        it('for producer', () => testDelete(
            StubObjects.producer,
            PSLambdas.addProducer,
            PSLambdas.findProducer,
            PSLambdas.deleteProducer
        ));
        it('for project', () => testDelete(
            StubObjects.project,
            PSLambdas.addProject,
            PSLambdas.findProject,
            PSLambdas.deleteProject
        ));
    });
    describe('should support update operation', () => {
        it('for customer', () => testUpdate(
            StubObjects.customer, StubObjects.customer2,
            PSLambdas.addCustomer,
            PSLambdas.findCustomer,
            PSLambdas.updateCustomer
        ));
        it('for producer', () => testUpdate(
            StubObjects.producer, StubObjects.producer2,
            PSLambdas.addProducer,
            PSLambdas.findProducer,
            PSLambdas.updateProducer
        ));
        it('for project', () => testUpdate(
            StubObjects.project, StubObjects.project2,
            PSLambdas.addProject,
            PSLambdas.findProject,
            PSLambdas.updateProject
        ));
    });

    describe('should support project assigning', () => {
        it('to customer', () => {
            const projectService = new ProjectService();

            const customer = projectService.addCustomer(StubObjects.customer);
            const project = projectService.rawAddProject(StubObjects.project);
            const projectWithCustomer = projectService.addProjectToCustomer(customer.id as number, project.id as number);

            assert.notEqual(projectWithCustomer, null);
            assert.deepStrictEqual((projectWithCustomer as ExpandedProject).customer, customer);
        });
        it('to producer', () => {
            const projectService = new ProjectService();

            const producer = projectService.addProducer(StubObjects.producer);
            const project2 = projectService.rawAddProject(StubObjects.project2);
            const projectWithProducer = projectService.addProjectToProducer(producer.id as number, project2.id as number);

            assert.notEqual(projectWithProducer, null);
            assert.deepStrictEqual((projectWithProducer as ExpandedProject).producer, producer);
        });
        it('to both producer and customer', () => {
            const projectService = new ProjectService();

            const producer = projectService.addProducer(StubObjects.producer);
            const customer = projectService.addCustomer(StubObjects.customer);
            const project = projectService.rawAddProject(StubObjects.project);
            
            projectService.addProjectToCustomer(customer.id as number, project.id as number);
            projectService.addProjectToProducer(producer.id as number, project.id as number);

            const assignedProject = projectService.findProject(project.id as number);

            assert.notEqual(assignedProject, null);
            assert.deepStrictEqual((assignedProject as ExpandedProject).customer, customer);
            assert.deepStrictEqual((assignedProject as ExpandedProject).producer, producer);
        });
    });
});

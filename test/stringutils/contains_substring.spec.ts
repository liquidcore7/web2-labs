import { containsSubstring } from '../../src/stringutils/contains_substring';
import * as assert from 'assert';


describe('Substring search', () => {
    it('should return false on non-match', () => {
        assert.strictEqual(containsSubstring('We the people of United States', 'me'), false);
    });
    it('should detect a prefix match', () => {
        assert.strictEqual(containsSubstring('Passport', 'Pass'), true);
    });
    it('should detect word ending match', () => {
        assert.strictEqual(containsSubstring('Cheeseburger', 'burger'), true);
    });
    it('should detect suffix match', () => {
        assert.strictEqual(containsSubstring('NullPointerException', 'Pointer'), true);
    });
});

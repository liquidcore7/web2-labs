import { splitByWord } from '../../src/stringutils/split_string';
import * as assert from 'assert';


describe('Splitting by word', () => {
    it('should return empty array for empty string', () => {
        assert.deepStrictEqual(splitByWord(''), []);
    });
    it('should return one-element array for single unpadded word', () => {
        assert.deepStrictEqual(splitByWord(' withLeadingSpace'), ['withLeadingSpace']);
    });
    it('should split a normal sentence into words', () => {
        assert.deepStrictEqual(
            splitByWord('There is a specter haunting Europe, ...'), 
            ['There', 'is', 'a', 'specter', 'haunting', 'Europe,', '...']
        );
    });
    it('should trim extra spaces while splitting', () => {
        assert.deepStrictEqual(
            splitByWord('INSERT  COIN'),
            ['INSERT', 'COIN']
        );
    });
});

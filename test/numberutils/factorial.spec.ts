import { factorial } from '../../src/numberutils/factorial';
import * as assert from 'assert';


describe('Factorial', () => {
    it('should return one for zero', () => {
        assert.strictEqual(factorial(0), 1);
    });
    it('should be correctly calculated for small positive numbers', () => {
        assert.strictEqual(factorial(5), 120);
    });
    it('should be correctly calculated for bigger positive numbers', () => {
        assert.strictEqual(factorial(13), 6227020800);
    });
});
import * as assert from 'assert';
import { bubbleSorted } from '../../src/arrayutils/bubble_sort';


describe('Bubble sort', () => {
    it('should return empty array on empty input', () => {
        assert.deepStrictEqual(bubbleSorted([]), []);
    });
    it('should sort descending order number array', () => {
        assert.deepStrictEqual(bubbleSorted([3, 2, 1, 0, -1]), [-1, 0, 1, 2, 3]);
    });
    it('should sort string array lexicographically', () => {
        assert.deepStrictEqual(
            bubbleSorted(['a', 'bb', 'b', 'aa', 'abc', 'cda']),
            ['a', 'aa', 'abc', 'b', 'bb', 'cda']
        );
    });
});
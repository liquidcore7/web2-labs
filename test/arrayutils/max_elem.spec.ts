import { maxElement } from '../../src/arrayutils/max_elem';
import * as assert from 'assert';


describe('maxelem', () => {
    it('should throw on empty array', () => {
        assert.throws(() => maxElement([]));
    });
    it('should work on primitive types, deriving the comparator', () => {
        assert.strictEqual(
            maxElement([-1, 4, 2, 8, 9, 111, -342, 402, 498, -299, 38]),
            498
        );
    });
    it('should use the custom comparator when provided', () => {
        assert.strictEqual(
            maxElement(['abc', 'cde', 'efg', 'wwww', 'cc'], (l: string, r: string): boolean => l.length < r.length),
            'wwww'
        );
    });
});
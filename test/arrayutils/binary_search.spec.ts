import { binarySearch } from '../../src/arrayutils/binary_search';
import * as assert from 'assert';


describe('Binary search', () => {
    it('should return null on empty array', () => {
        assert.equal(
            binarySearch([], undefined),
            null
        )
    });
    it('should return the index of Array.head if the only element of an array is searched', () => {
        assert.strictEqual(binarySearch(['abc'], 'abc'), 0);
    });
    it('should operate on even-sized arrays', () => {
        assert.strictEqual(
            binarySearch(['0e', '1e', '2e', '3e', '4e', '5e'], '3e'),
            3
        );
    });
    it('should operate on odd-sized arrays', () => {
        assert.strictEqual(
            binarySearch([-2, -1, 0, 1, 2], 2),
            4
        );
    });
});